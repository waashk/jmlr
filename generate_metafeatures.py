from sklearn.datasets import load_svmlight_file
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.datasets import dump_svmlight_file
from scipy.spatial.distance import cdist

def get_data(dataset):
    data = load_svmlight_file(dataset)
    return data[0], data[1]

k_folds=5
dataset="webkb"

for k in range(k_folds):
#Treino
	X_treino, y_treino = get_data("original/"+dataset+"/treino"+str(k)+"_orig")
	classes = list(set(map(int,y_treino)))
	n_classes = len(classes)
	
	docs_by_class = []
	X_by_class = []
	knn_by_class = []
	
	for i in classes:
	    X_tmp = X_treino[np.where(y_treino == i)]
	    knn = NearestNeighbors(n_neighbors=31, algorithm="brute", metric='cosine')
	    knn.fit(X_tmp)
	    #knn.kneighbors(X_treino_0[0], 31, return_distance=True)
	    knn_by_class.append(knn)
	
	metafeatures = []
	#classes deve começar com 0
	for i, doc in enumerate(X_treino):
	    for j in classes:
	        scores = np.cos(knn_by_class[j].kneighbors(X_treino[i], 31, return_distance=True)[0])[0]
	        #retira o primeiro ou o ultimo
	        if y_treino[i] == j:
	            scores = scores[1:]
	        else:
	            scores = scores[:-1]
	        metafeatures += list(scores)
	dump_svmlight_file(np.array(metafeatures).reshape((X_treino.shape[0],30*n_classes)),y_treino, f="metafeatures/"+dataset+"/treino"+str(k)+"_orig")
	
	#Teste
	X_teste, y_teste = get_data("original/"+dataset+"/teste"+str(k)+"_orig")
	
	metafeatures_teste = []
	#classes deve começar com 0
	for i, doc in enumerate(X_teste):
	    for j in classes:
	        scores = np.cos(knn_by_class[j].kneighbors(X_teste[i], 30, return_distance=True)[0])[0]
	        #retira o primeiro ou o ultimo
	        metafeatures_teste += list(scores)
	dump_svmlight_file(np.array(metafeatures_teste).reshape((X_teste.shape[0],30*n_classes)),y_teste, f="metafeatures/"+dataset+"/teste"+str(k)+"_orig")
	
	
	#Centroid-based
	classes = list(set(map(int,y_treino)))
	
	for i in classes:
	    docs_by_class.append(len(np.where(y_treino == i)[0]))
	
	centroids = []
	for i in classes:
	    centroids.append(np.mean(X_treino[np.where(y_treino == i)],axis=0,dtype=np.float64))
	    
	centroids_sum = []
	for i in classes:
	    centroids_sum.append(np.sum(X_treino[np.where(y_treino == i)],axis=0,dtype=np.float64))
	    
	#Treino
	metafeatures_centroid = []
	for i, doc in enumerate(X_treino):
	    for j in classes:
	        if y_treino[i] == j:
	            c = (centroids_sum[j] - X_treino[i])/(docs_by_class[j]-1)
	        else:
	            c = centroids[j]
	        metafeatures_centroid += [cdist(X_treino[i].toarray() ,c,metric='cosine')[0][0]]
	
	dump_svmlight_file(np.array(metafeatures_centroid).reshape((X_treino.shape[0],n_classes)),y_treino, f="metafeatures/"+dataset+"/treino"+str(k)+"_orig_centroidbased", zero_based=False)
	
	#Teste
	metafeatures_centroid_teste = []
	
	#X_teste, y_teste = get_data("original/webkb/teste0_orig")
	
	#classes deve começar com 0
	for i, doc in enumerate(X_teste):
	    for j in classes:
	        c = centroids[j]
	        metafeatures_centroid_teste += [cdist(X_teste[i].toarray() ,c,metric='cosine')[0][0]]
	
	dump_svmlight_file(np.array(metafeatures_centroid_teste).reshape((X_teste.shape[0],n_classes)),y_teste, f="metafeatures/"+dataset+"/teste"+str(k)+"_orig_centroidbased", zero_based=False)